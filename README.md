## Task description : 

Pls fork this project to youw own repo or create a separate branch

There is a small project done by our POs for an internal use.
You task is to make it production-ready. 

You can find project description in project_details.md file

Pls make sure that:
* Lambas will be propertly triggered. 
* IAM roles are limited to a minimal necessary permissions. 
* All AWS resouces are configured according to AWS best practices.
* Serverless project is configured according to Serverless framework best practices.
* The whole setup is secure, scalable and reusable.
* Project can de deployed to dev/tst/acc and prod envs.
* If some functionality is not natibly supported by AWS/Serverless framework it should be provided my custom scripts.

## Bonus tasks
* Setup GitLab CICD pipeline (as far as it possible in one account setup)
* Add tests to verify infrastructure setup and notify developers in case of issues.
* Improve current setup: logs, metrics, API docs and etc 


Good luck! 