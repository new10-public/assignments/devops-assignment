import json, boto3, os

s3 = boto3.client('s3')
BUCKET=os.environ['BUCKET']

def get_gcrm_risk_class(event, context):
    body={} if event['body']==None else json.loads(event['body'])
    data_model = {
        "keys": {
            "sbi": {
                'required': True,
                'type': str
            }
        }
    }
    success, body = validate(data_model, body)
    if not success: return return_response(400, body)

    gcrm_mapping=get_dict_from_s3(BUCKET, 'companies/gcrm_classification.json')
    if body["sbi"] not in gcrm_mapping: return return_response(400, "unknown SBI")

    return return_response(200, gcrm_mapping[body["sbi"]])

def get_dict_from_s3(bucket, key):
    data = s3.get_object(Bucket=bucket, Key=key)
    return json.loads(data['Body'].read())

def return_response(code, val):
    return {
        'statusCode': code,
        'body': json.dumps(val),
        'headers': {'Content-Type': 'application/json'}}

def validate(model,request):
    for i in model['keys']:
        if i not in request:
            if model['keys'][i]['required']:
                return False, f'missing mandatory key {i}'
            elif 'preset' in model['keys'][i]:
                request[i]=model['keys'][i]['preset']
        else:
            if type(request[i])!=model['keys'][i]['type']:
                return False, f"wrong type for {i}, should be {model['keys'][i]['type']}, found {type(request[i])}"
            if 'list' in model['keys'][i] and request[i] not in model['keys'][i]['list']:
                return False, f"wrong value for {i}, should be one of {model['keys'][i]['list']}, found {request[i]}"
            if 'range' in model['keys'][i] and request[i] not in range(model['keys'][i]['range'][0],model['keys'][i]['range'][1]+1):
                return False, f"wrong value for {i}, should be in the range {model['keys'][i]['range']}, found {request[i]}"
    return True, request
