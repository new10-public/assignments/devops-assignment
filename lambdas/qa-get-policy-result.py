import json, boto3, os

s3 = boto3.client('s3')
BUCKET= os.environ['BUCKET']
POLICIES= os.environ['FILE']

def handler(event, context):
    event = {} if event['body'] == None else json.loads(event['body'])
    policies_json=get_dict_from_s3(BUCKET, POLICIES)

    P=policies(policies_json)
    if "values" not in event: event['values']={}
    result=P.evaluate(event['values'])

    if "status" in event and event["status"]:
        return return_response(200, P.getstatus())

    if "verbose" in event and not event["verbose"]:
        result={i:{
            "failed":result[i]['failed'],
            "message": result[i]['policy']['message'],
            "missing dependencies":result[i]["missing dependencies"]
        } for i in result}

    if "unsuccessful_only" in event and event["unsuccessful_only"]:
        result={i:result[i] for i in result if result[i]['failed'] in [True, None]}

    return return_response(200, result)

def return_response(code, val):
    return {'statusCode': code, 'body': json.dumps(val),
            'headers': {'Content-Type': 'application/json'}}

def get_dict_from_s3(bucket, key):
    data = s3.get_object(Bucket=bucket, Key=key)
    return json.loads(data['Body'].read())

class policy:
    def __init__(self,id,policy,dependencies):
        self.id=id
        self.policy=policy
        self.dependencies=dependencies

    def evaluate(self,payload):
        r= {"input": payload, "policy": self.policy, "failed": False, "missing dependencies":[]}
        for i in self.dependencies:
            if i not in payload.keys():
                r["failed"]= None
                r["missing dependencies"].append(i)
        if len(r["missing dependencies"])==0:
            for i in payload:
                if type(payload[i])==str: exec(f"{i}='{payload[i]}'")
                else:                     exec(f"{i}={payload[i]}")
            for rule in self.policy["rules"]:  r["failed"]=eval(rule)
        return r

class policies:
    def __init__(self,policies_json):
        self.version = policies_json["versioning"]["version"]
        self.updated = policies_json["versioning"]["updated"]
        self.dependencies={}
        for i in policies_json["dependencies"]:
            for j in policies_json["dependencies"][i]:
                if j not in self.dependencies: self.dependencies[j]=[i]
                else:                          self.dependencies[j].append(i)
        self.policies=[policy(p, policies_json["policies"][p],self.dependencies[p]) for p in policies_json["policies"]]
        self.outcome=None

    def evaluate(self,payload):
        self.outcome = {policy.id:policy.evaluate(payload) for policy in self.policies}
        return self.outcome

    def getstatus(self):
        missing_dependencies=[]
        untested_rules=[]
        failed_rules=[]
        sucess_rules=[]
        total=0
        for policy in self.outcome:
            total+=1
            if self.outcome[policy]['failed']==None:
                for i in self.outcome[policy]["missing dependencies"]:
                    if i not in missing_dependencies: missing_dependencies.append(i)
                untested_rules.append(policy)
            if self.outcome[policy]['failed']==True:
                failed_rules.append([policy,self.outcome[policy]['policy']['message'],self.outcome[policy]['policy']['rules']])
            if self.outcome[policy]['failed']==False:
                sucess_rules.append(policy)
            summary={"total":total, "success":len(sucess_rules),"failed":len(failed_rules),"untested": len(untested_rules),"accept":len(sucess_rules)==total}
        return {"summary":summary, "untested rules": untested_rules, "failed rules": failed_rules, "missing dependencies": missing_dependencies}
