import json

ACCOUNT_F=  '/Users/ruben/Downloads/cmp_account_201803291113.json'
ADDRESS_F=  '/Users/ruben/Downloads/cmp_address_201803291114.json'
SBI_F=      '/Users/ruben/Downloads/cmp_sbi_activity_201803291115.json'
OUTPUT_F=   '/Users/ruben/Downloads/cmp.json'
mapping={'EENMANSZAAK':'EMZ','BESLOTEN_VENNOOTSCHAP':'BV','VENNOOTSCHAP_ONDER_FIRMA':'VOF'}

ACCOUNT={i['id']:i  for i in json.loads(open(ACCOUNT_F,'r').read())['cmp_account']}
ADDRESS={i['accountId']:i  for i in json.loads(open(ADDRESS_F,'r').read())['cmp_address']}
SBI={i['accountId']:i  for i in json.loads(open(SBI_F,'r').read())['cmp_sbi_activity']}

result={
    ACCOUNT[i]["name"]:{
        'name':         ACCOUNT[i]["name"],
        'legal form':   mapping[ACCOUNT[i]["legalForm"]],
        'zip':          ADDRESS[i]["postalCode"],'sbi':SBI[i]["code"],
        'founded':      ACCOUNT[i]['dateOfIncorporation'][:10]
    } for i in ACCOUNT
}
with open(OUTPUT_F,'w') as A:
    A.write(json.dumps(result, sort_keys=True, indent=4, separators=(',', ': ')))
